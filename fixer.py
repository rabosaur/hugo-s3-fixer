#!/bin/python
from contextlib import contextmanager
from bs4 import BeautifulSoup
import argparse
import os
import sys

@contextmanager
def pushd(newDir):
    """ this is to handle changing directory for a block and restoring the directory
    @param newDir: directory to cd to and put on the call stack
    when the block has exited."""
    #ref: http://stackoverflow.com/questions/6194499/python-os-system-pushddef
    previousDir = os.getcwd()
    os.chdir(newDir)
    yield
    os.chdir(previousDir)


def parse_command_line():
    """ parse the command line and verify argument combinations. exit if
    invalid combinations are selected """
    parser = argparse.ArgumentParser(description="Fix hugo output so it works in AWS S3")
    # add verbosity command line argument
    parser.add_argument("-p", "--path", dest="path",
                        help="Path to the Hugo generated public folder", required=True)
    args = parser.parse_args()
    return args


def get_all_files():
    f = []
    for root, dirs, files in os.walk('.'):
        f.extend([os.path.join(root, x) for x in files])
    # got all the files, but lets only look at html files
    f = [x for x in f if x.endswith(".html")]
    return f


def find_and_edit_tags(file_name):
    print("Processing {fn}".format(fn=file_name))
    local_links = []
    if not os.path.split(file_name)[-1].startswith("."):
        with open(file_name, "rb") as f:
            lines = f.readlines()
        line = ''.join(lines)
        soup = BeautifulSoup(line, "html.parser")
        local_links = [x for x in soup.find_all("a") if "href" in x.attrs.keys()]
        for l in local_links:
            orig_url = url = l['href']
            if url != '/':
                if url.startswith("/") and not url.endswith(".html") and "index.html" not in url:
                    if not url.endswith('/'):
                        url += "/"
                    l['href'] = url + "index.html"
        if local_links:
            html = soup.prettify(soup.original_encoding)
            with open(file_name, "wb") as file:
                file.write(html)
    return local_links


def main():
    args = parse_command_line()
    if not (os.path.exists(args.path) or os.path.isdir(args.path)):
        print("{p} is not a valid directory".format(p=args.path))
        return 1
    with pushd(args.path):
        find_and_edit_tags("./index.html")
        for file_name in get_all_files():
            find_and_edit_tags(file_name)
    print "done"
    return 0


# ####################MAIN ENTRY POINT AS A SCRIPT ################
if __name__ == '__main__':
    sys,exit(main())

